<?php



$request = $_SERVER['REQUEST_URI'];

switch ($request) {

    case '':
    case '/':
        require_once('Controller/IndexController.php');
        break;

    case '/newgame':
        $gameController = new \App\Controller\GameController();
        $gameController->createNewGame();
        break;

    case '/nextRound':
        $gameController = new \App\Controller\GameController();
        $gameController->nextRound();
        break;

    default:
        http_response_code(404);
        require __DIR__ . '/views/404.php';
        break;
}