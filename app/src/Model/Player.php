<?php

namespace App\Model;


class Player
{

    private Knight $Knight;
    private int $id;

    /**
     * @param Knight $knight
     */
    public function __construct(int $id,Knight $knight)
    {
        $this->Knight = $knight;
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return Knight
     */
    public function getKnight(): Knight
    {
        return $this->Knight;
    }

}