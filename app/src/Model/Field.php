<?php

namespace App\Model;


use App\Core\SessionManager;

class Field
{

    /**
     * @var Player[]
     */
    private array $activePlayers;
    private SessionManager $session;

    /**
     * @return Player[]
     */
    public function getActivePlayers(): array
    {
        $activePlayers = array();
        foreach ($this->activePlayers as $id => $player) {
            $knight = $player->getKnight();
            if ($knight->isDead()) continue;
            $activePlayers[] = array(
                'id' => $player->getId(),
                'health' => $knight->getHealth(),
                'isDead' => $knight->isDead()
            );
        }
        return $activePlayers;
    }


    /**
     * initialise a game with the number of players
     * @param int $players
     */
    public function __construct(int $players = 5)
    {
        $this->session = new SessionManager();

        for ($id = 0; $id < $players; $id++) {
            $this->activePlayers[] = new Player($id, new Knight());
        }
    }


    /**
     * calculate the damage dealt by the player in round x
     * @param int $playerAttackingId
     * @return array
     */
    public function nextFight(int $playerAttackingId): array
    {
        $playerAttackingIndex = $this->getPlayerIndexById($playerAttackingId);
        $playerDefendingIndex = $this->getNextActivePlayer($playerAttackingIndex);
        $damage = Dice::roll();
        $playerDefending = $this->activePlayers[$playerDefendingIndex];
        $knight = $this->activePlayers[$playerDefendingIndex]->getKnight();
        $knightCurrentHealth = $knight->getHealth();
        $knight->setHealth($knightCurrentHealth - $damage);
        if ($knight->isDead()) {
            unset($this->activePlayers[$playerDefendingIndex]);
        }
        if (count($this->activePlayers)>1){
            $nextRoundAttackingPlayer = $this->getNextActivePlayer($playerAttackingIndex);
            $this->session->set('playerAttackingId', $this->activePlayers[$nextRoundAttackingPlayer]->getId());
        }
        return ['playerAttacking' => $playerAttackingId, 'damage' => $damage, 'playerDefending' => $playerDefending->getId()];

    }

    /**
     * get next Index of alive player in our players array
     * @param int $index
     * @return int
     */
    private function getNextActivePlayer(int $index): int
    {
        do {
            $index++;
            $activePlayerIndex = fmod($index, count($this->activePlayers));
        } while (!isset($this->activePlayers[$activePlayerIndex]));

        return $activePlayerIndex;

    }

    /**
     * @param int $playerAttackingId
     * @return bool|int|string
     */
    private function getPlayerIndexById(int $playerAttackingId): bool|int|string
    {
        return array_search($playerAttackingId, array_column($this->getActivePlayers(), 'id'));
    }

}