<?php

namespace App\Core;

class Request
{

    public static function getRequest(string $name = '',$default = null){
        if (empty($name)){
            return null;
        }
        if (isset($_POST[$name])){
            return $_POST[$name];
        }
        else if (isset($_GET[$name])){
            return $_POST[$name];
        }
        else {
            return $default;
        }

    }
}